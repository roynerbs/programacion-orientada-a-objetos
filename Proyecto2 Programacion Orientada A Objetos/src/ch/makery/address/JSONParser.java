package ch.makery.address;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import ch.makery.address.model.Lugar;
import ch.makery.address.model.Usuario;


public class JSONParser {
	
	private File file;
	private ObjectMapper mapper;
	private JsonNode nodo;
	
	public JSONParser() {
		file = new File("data/data.json");
		mapper = new ObjectMapper();
		
		try {
			nodo = mapper.readTree(file);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

		
		public Sistema cargarUsuarios() {
			
			Sistema sistema = new Sistema();
			
			ArrayNode usuarios = (ArrayNode) nodo.get("usuarios");
			if(usuarios != null) {
				for(int i=0; i<usuarios.size();i++) {
					JsonNode usuario = usuarios.get(i);
					String nombre = usuario.get("nombre").asText();
					String contrasena = usuario.get("contrasena").asText();
					String provincia = usuario.get("provincia").asText();
					String canton = usuario.get("canton").asText();
					String distrito = usuario.get("distrito").asText();
					int placa = usuario.get("placa").asInt();
					Usuario usr = new Usuario(nombre, contrasena,provincia,canton,distrito,placa);
					sistema.add(usr);
					}
				}
			return sistema;
			}
		
		
		//Busca un usuario y devuelve un i segun el lugar donde esta, una definir una Sesion...
		public int buscarUsuarios(String Usuario, String Contrasena) {
			
		
			
			ArrayNode usuarios = (ArrayNode) nodo.get("usuarios");
			if(usuarios != null) {
				int i = 0;
				while (i < usuarios.size()) {
					JsonNode usuario = usuarios.get(i);
					String nombre = usuario.get("nombre").asText();
					String contrasena = usuario.get("contrasena").asText();
					if (nombre.equals(Usuario) & contrasena.equals(Contrasena)) {
						return i;
					}
					else {
						i+=1;
					}
					
				}
			}
			return -1;
		}
		
}

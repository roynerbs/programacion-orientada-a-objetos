package ch.makery.address;


import ch.makery.address.model.Usuario;
import ch.makery.address.view.DisplayController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;


public class MainApp extends Application {
	
	
	
	
	public MainApp() {
		
		
	}
	

	
	private Stage Display;
	//Carga primera escena.
	@Override
	public void start(Stage Display) {
		try {
			 Parent root = FXMLLoader.load(getClass().getResource("view/Inicio.fxml"));
			 
			 Scene scene = new Scene(root);
			 
			 Display.setScene(scene);
			 Display.show();
			
		    }
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	 

	public static void main(String[] args) {
		launch(args);
	}
}

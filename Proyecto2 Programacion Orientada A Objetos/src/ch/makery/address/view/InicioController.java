package ch.makery.address.view;

import java.io.IOException;

import ch.makery.address.JSONParser;
import ch.makery.address.JSONRead;
import ch.makery.address.Sistema;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class InicioController {
	
	@FXML
	private TextField Username;
	
	@FXML
	private PasswordField Password;
	
	@FXML
	private Label Fail;
	
	
	@FXML
	private void initialize() 
	{
		
		
	}
	public void cambiarRegistro(ActionEvent event ) throws IOException
	{
		Parent RegistroPadre = FXMLLoader.load(getClass().getResource("Registro.fxml"));
		Scene Registro = new Scene(RegistroPadre);
		
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
		window.setScene(Registro);
		window.show();
	}
	
	public void cambiarInfo(ActionEvent event ) throws IOException
	{
		int SesionActual = usuarioValido();
		
		if(SesionActual >= 0) {	
			//carga
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Display.fxml"));
            Parent InfoPadre = loader.load();
            Scene Info = new Scene(InfoPadre);
             
            //utiliza metodo del controller
            DisplayController Envios = loader.getController();
            Envios.determinarSesion(SesionActual);

            //muestra
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
			window.setScene(Info);
			window.show();
		}
		else {
			Fail.setText("Usuario o Contrasena Incorrectos");
		}
	}
	
	public int usuarioValido() //lee el json de usuarios para comparar contrasena y usuario.
	{
		JSONParser pars = new JSONParser();
		
		
		String Usuario = Username.getText();
		String Contrasena = Password.getText();
		return pars.buscarUsuarios(Usuario, Contrasena);
	}
	
	
}

package ch.makery.address.view;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.makery.address.model.Usuario;
import ch.makery.address.model.UsuarioJSON;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class RegistroController {
	
	@FXML
	private TextField nombreUsuario;
	
	@FXML
	private PasswordField contraUsuario;
	
	@FXML
	private TextField provinciaUsuario;
	
	@FXML
	private TextField cantonUsuario;
	
	@FXML
	private TextField distritoUsuario;
	
	@FXML
	private TextField placaUsuario;
	@FXML
	private Label Error;
	
	@FXML
	private void initialize() 
	{
		
	}
	
	
	
	
	public void cambiarInicio(ActionEvent event ) throws IOException
	{
		if(registrarUsuario()==true) {
		Parent InicioPadre = FXMLLoader.load(getClass().getResource("Inicio.fxml"));
		Scene Inicio = new Scene(InicioPadre);
		
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
		window.setScene(Inicio);
		window.show();
		}
		else {
		}
	}
	
	public boolean registrarUsuario() throws JsonProcessingException, IOException
	{
		if(verificarFields()==true) {
			String name = nombreUsuario.getText();
			String pass = contraUsuario.getText();
			String prov = provinciaUsuario.getText();
			String cant = cantonUsuario.getText();
			String dist = distritoUsuario.getText();
			String pla = placaUsuario.getText();
			int plac;
			try {
				
					plac = Integer.parseInt(pla);
					
					//escribe en json una clase.
					
					UsuarioJSON user = new UsuarioJSON(name,pass,prov,cant,dist,plac);
					ObjectMapper mapper = new ObjectMapper();
		            File file = new File("data/data.json");
		            mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		            JsonNode nodo = mapper.readTree(file);
		            ArrayNode usuarios = (ArrayNode) nodo.get("usuarios");
		            JsonNode newNode = mapper.valueToTree(user);
		            System.out.println(newNode);
		            usuarios.add(newNode);
		            ((ObjectNode) nodo).set("usuarios", usuarios);
		            mapper.writeValue(file, nodo);
		            return true;
				}
				catch (NumberFormatException e)
				{
					Error.setText("Porfavor rellenar todos los espacios correctamente");
					return false;
				}
		}
		else {
			Error.setText("Porfavor rellenar todos los espacios correctamente");
			return false;
		}
		
	}




	public boolean verificarFields() {
		if(nombreUsuario.getText().trim().isEmpty() || contraUsuario.getText().trim().isEmpty() || provinciaUsuario.getText().trim().isEmpty()  || cantonUsuario.getText().trim().isEmpty()  || distritoUsuario.getText().trim().isEmpty() || placaUsuario.getText().trim().isEmpty()) {
			return false;
		}
		else {
			return true;
		}
	}

}

package ch.makery.address.view;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import ch.makery.address.JSONParser;
import ch.makery.address.JSONRead;
import ch.makery.address.MainApp;
import ch.makery.address.Sistema;
import ch.makery.address.model.Usuario;


public class DisplayController {
	
	@FXML
    private Label Nombre;
	
	@FXML
    private Label Lugar;
    
    @FXML
    private Label Placa;
    
    @FXML
    private Label Alerta;
    
    
    
    private int Sesion;
    
    
    

    @FXML
    private void initialize() {
    	
    	
    	
    	
    }
    
    private void establecerInfo() {
    	JSONParser parser = new JSONParser();
		Sistema sistema = parser.cargarUsuarios();
  
    	//establece info segun la sesion.
		
    	Nombre.setText(sistema.get(Sesion).getNombre());
    	
    	
        Lugar.setText(sistema.get(Sesion).getLugar().toString() + " se encuentra en " + determinarAlerta(sistema));
        
        //pasar a string un int xd
        int placatemp = sistema.get(Sesion).getPlaca();
        String placa = String.valueOf(placatemp);
        
        
        Placa.setText("El vehiculo con la placa " + placa + " puede circular: \n" + determinarCirculacion(sistema));
    }
    
    
    
    private String determinarAlerta(Sistema sistema) {
    	String provincia = sistema.get(Sesion).getLugar().getProvincia();
    	String canton = sistema.get(Sesion).getLugar().getCanton();
    	String distrito = sistema.get(Sesion).getLugar().getDistrito();
    	JSONRead buscador = new JSONRead();
    	if(buscador.buscar(provincia, canton, distrito) == true) {
    		Alerta.setText(naranja());
    		return "alerta naranja";
    	}
    	else {
    		Alerta.setText(amarilla());
    		return "alerta amarilla";
    	}
 
    }
    
    private String determinarCirculacion(Sistema sistema) 
    {
    	
    	//obtiene primer digito de la placa
    	String placatemp = Integer.toString(sistema.get(Sesion).getPlaca());
    	int largo = placatemp.length()-1;
    	int placa = sistema.get(Sesion).getPlaca()/((int) Math.pow(10,largo));
    	
    	
    	//definir el label segun digito de la placa
    	switch(placa) 
    	{
    	case 1:
    	case 2:{
    		String salida = "• Lunes: 5:00 AM - 5:00 PM \n";
    		if(placa == 2) {salida += "• Sabado: 5:00 AM - 5:00 PM";return salida;}
    		else {salida += "• Domingo: 5:00 AM - 5:00 PM (Abasto y salud)";return salida;}
    	}
    	
    	case 3:
    	case 4:{
    		String salida = "• Martes: 5:00 AM - 5:00 PM \n";
    		if(placa == 4) {salida += "• Sabado: 5:00 AM - 5:00 PM";return salida;}
    		else {salida += "• Domingo: 5:00 AM - 5:00 PM (Abasto y salud)";return salida;}
    	}
    	case 5:
    	case 6:{
    		String salida = "• Miercoles: 5:00 AM - 5:00 PM \n";
    		if(placa == 6) {salida += "• Sabado: 5:00 AM - 5:00 PM";return salida;}
    		else {salida += "• Domingo: 5:00 AM - 5:00 PM (Abasto y salud)";return salida;}
    	}
    	case 7:
    	case 8:{
    		String salida = "• Jueves: 5:00 AM - 5:00 PM \n";
    		if(placa == 8) {salida += "• Sabado: 5:00 AM - 5:00 PM";return salida;}
    		else {salida += "• Domingo: 5:00 AM - 5:00 PM (Abasto y salud)";return salida;}
    	}
    	case 9:
    	case 0:{
    		String salida = "• Viernes: 5:00 AM - 5:00 PM \n";
    		if(placa == 0) {salida += "• Sabado: 5:00 AM - 5:00 PM";return salida;}
    		else {salida += "• Domingo: 5:00 AM - 5:00 PM (Abasto y salud)";return salida;}
    	}
    	}
		return "algo, sino marca en rojo aunque arriba retorno una salida que es string xd";
    }
    
    
    private String naranja() {
    	return "• Supermercados, farmacias, centros médicos y pulperías.\n" + 
    			"• Abastecedores, panaderías, carnicerías y verdulerías.\n" + 
    			"• Suministros agropecuarios, veterinarios y de higiene,\n" + 
    			"• Servicio a domicilio y bancos ";
    	
    }
    private String amarilla() {
    	return "• Salones de belleza, lugares de culto y playas de 5:00 am a 9:30 am\n" + 
    			"• Hoteles, restaurantes, tiendas, museos, \n" + 
    			"• Cines y teatros al 50% de aforo";
    }
    
    //se accede a este metodo desde el controlador inicio, para definir una sesion ademas de establecer la info pertinente a los labels.
    public void determinarSesion(int Sesion) {
    	this.Sesion = Sesion;
    	establecerInfo();
    	
    }
    


}




package ch.makery.address;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class JSONRead {
	private File archivo;
	private ObjectMapper mapp;
	private JsonNode nodo;
	
	public JSONRead() {
		
		archivo = new File("data/alertanaranja.json");
		mapp = new ObjectMapper();
		
		try {
			nodo = mapp.readTree(archivo);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	//Funciones cargan y leen el json sin necesidad de crear objetos basados en clases funcionales.
    
    //Recibe 3 parametros: provincia, canton y distrito, primero busca la provincia y busca canton en base al indice de provincia,
	//ex: San Jose = 1, Alajuela = 2, etc. En el JSON hay un array con arrays para los cantones de cada provincia([[1][2][3][4][5][6][7]])
	//finalmente busca distrito, se ve que los distritos son ciertas excepciones las que no cumplen la alerta de su canton.
	//Es por esto que evalua false. True para alerta naranja, False para alerta amarilla.
	
	
	public boolean buscar(String provincia, String canton, String distrito) {
		
		ArrayNode provincias = (ArrayNode) nodo.get("provincias"); 
		if(provincias != null) {
			int i = 0;
			while (i < provincias.get(0).size()) {
				String key = String.valueOf(i+1);
				JsonNode alertaprov = provincias.get(0);
				String Provincia = alertaprov.get(key).asText();
				if (Provincia.equals(provincia)) {
					i+=1;
					if(buscarCanton(canton, i) == true & buscarDistrito(distrito)==false ) {
						return true;
					}
					else {
						return false;
					}
				}
				else {
					i+=1;
				}
				
			}
		}
		return false;
	}
	
	
	public boolean buscarCanton(String canton, int indice) {
		ArrayNode cantones = (ArrayNode) nodo.get("cantones");
		String llave = String.valueOf(indice);
		if(cantones != null) {
			int j = 0;
			while (j < cantones.get(0).get(llave).size()) {
				JsonNode alertacant = cantones.get(0).get(llave);
				String Canton = alertacant.get(j).asText();
				if (Canton.equals(canton)) {
						return true;
					}
				else {
					j+=1;
				}			
			}
				
		}	
		return false;
	}
	
	public boolean buscarDistrito(String distrito) {
		ArrayNode distritos = (ArrayNode) nodo.get("distritos");
		if(distritos != null) {
			int i = 0;
			while (i < distritos.get(0).size()) {
				String clave = String.valueOf(i+1);
				JsonNode alertadist = distritos.get(0);
				String Distrito = alertadist.get(clave).asText();
				if (Distrito.equals(distrito)) {
						return true;
					}
				else {
					i+=1;
				}
			}	
		}
		return false;
	}
	


}

package ch.makery.address.model;


//Clase con la unica funcionalidad de escribir datos en el json destinado a usuarios.
//Se puede reducir, quitar clase Lugar y esta clase, implementar Usuario como se implementa esta, y sigue teniendo la misma funcionalidad
//ademas de menos recursos, creo...

public class UsuarioJSON {
	
	private String Nombre;
	private String Contrasena;
	private String Provincia;
	private String Canton;
	private String Distrito;
	private int Placa;
	

	public UsuarioJSON() 
	{
	}
	
	
	public UsuarioJSON(String Nombre,String Contrasena,String Provincia, String Canton, String Distrito,int Placa) 
	{
		this.Nombre = Nombre;
		this.Contrasena = Contrasena;
		this.Provincia = Provincia;
		this.Canton = Canton;
		this.Distrito = Distrito;
		this.Placa = Placa;
	}


	public String getNombre() {
		return Nombre;
	}


	public void setNombre(String nombre) {
		Nombre = nombre;
	}


	public String getContrasena() {
		return Contrasena;
	}


	public void setContrasena(String contrasena) {
		Contrasena = contrasena;
	}


	public String getProvincia() {
		return Provincia;
	}


	public void setProvincia(String provincia) {
		Provincia = provincia;
	}


	public String getCanton() {
		return Canton;
	}


	public void setCanton(String canton) {
		Canton = canton;
	}


	public String getDistrito() {
		return Distrito;
	}


	public void setDistrito(String distrito) {
		Distrito = distrito;
	}


	public int getPlaca() {
		return Placa;
	}


	public void setPlaca(int placa) {
		Placa = placa;
	}

}

package ch.makery.address.model;


public class Usuario {
	
	
	private String Nombre;
	private String Contrasena;
	private Lugar Lugar;
	private int Placa;
	

	public Usuario() 
	{
	}
	
	
	public Usuario(String Nombre,String Contrasena,String Provincia, String Canton, String Distrito,int Placa) 
	{
		this.Nombre= Nombre;
		this.Contrasena = Contrasena;
		Lugar lugar = new Lugar(Provincia,Canton,Distrito);
		this.Lugar = lugar;
		this.Placa= Placa;
	}
	

	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String Nombre) {
		this.Nombre = Nombre;
	}
	
	public String getContrasena() {
		return Contrasena;
	}
	public void setContrasena(String Contrasena) {
		this.Contrasena = Contrasena;
	}
	
	
	public int getPlaca() {
		return Placa;
	}
	public void setPlaca(int Placa) {
		this.Placa = Placa;
	}
	
	public Lugar getLugar() {
		return Lugar;
	}
	public void setLugar(Lugar lugar) {
		this.Lugar = lugar;
	}
	
	
}

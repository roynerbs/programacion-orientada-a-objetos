import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class JSONParser {
	
	private File file;
	private ObjectMapper mapper;
	private JsonNode nodo;
	
	public JSONParser() {
		file = new File("resources/data.json");
		mapper = new ObjectMapper();
		
		try {
			nodo = mapper.readTree(file);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Controlador cargarAparatos() {
		
		Controlador controlador = new Controlador();
		
		ArrayNode aparatos = (ArrayNode) nodo.get("aparatos");
		if(aparatos != null) {
			for(int i=0; i<aparatos.size();i++) {
				JsonNode aparato = aparatos.get(i);
				String nombre = aparato.get("nombre").asText();
				String marca = aparato.get("marca").asText();
				String tipo = aparato.get("tipo").asText();
				
				switch(tipo) {
					case "termoregulable":
						int minima = aparato.get("temperaturaMinima").asInt();
						int maxima = aparato.get("temperaturaMaxima").asInt();
						AparatoTermoRegulable atr = new AparatoTermoRegulable(nombre, marca, minima, maxima);
						controlador.add(atr);
						break;
					case "modalidades":
						ArrayNode modalidadesJSON = (ArrayNode) aparato.get("modalidades");
						if(modalidadesJSON != null) {
							String[] modalidades = new String[modalidadesJSON.size()];
							for(int j = 0; j < modalidadesJSON.size(); j++) {
								modalidades[j] = modalidadesJSON.get(j).asText();
							}
							AparatoConModalidad acm = new AparatoConModalidad(nombre, marca, modalidades);
							controlador.add(acm);
						}
						break;
					case "se�ales":
						ArrayNode se�alesJSON = (ArrayNode) aparato.get("se�ales");
						if(se�alesJSON != null) {
							float[]se�ales = new float[se�alesJSON.size()];
							for(int j = 0; j<se�alesJSON.size(); j++) {
								se�ales[j] = se�alesJSON.get(j).asLong();
							}
							AparatoConSe�al acs = new AparatoConSe�al(nombre,marca,se�ales);
							controlador.add(acs);
						}
						break;
				}
			}
		}
		return controlador;
	}
}

import java.util.Random;

public class AparatoConSe�al extends Aparato {

	private float[] se�ales;
	private float se�alActual;
	
	
	public AparatoConSe�al(String nombre, String marca, float[] se�ales) {
		super(nombre, marca);
		this.se�ales = se�ales;
	}


	@Override
	public void encender() {
		super.encender();
		Random ran = new Random();
		int aleatorio = ran.nextInt(se�ales.length);
		this.se�alActual = se�ales[aleatorio];
		System.out.println(super.toString() + " se�al actual " + se�alActual );
		
	}


	@Override
	public void apagar() {
		super.apagar();
		System.out.println(super.toString());
	}
	
	
	
	
}


public class DenunciaEspecial extends Denuncias {
	
	private double latitud;
	private double longitud;
	
	public double getlatitud() {
		return latitud;
	}
	
	public double getlongitud() {
		return longitud;
	}
	

	public DenunciaEspecial() {
		
	}
	
	public DenunciaEspecial(String descripcion, int fecha, int hora, double latitud, double longitud) {
		super(descripcion, fecha, hora);
		this.latitud = latitud;
		this.longitud = longitud;
	}
	
	@Override
	public String toString() {
		return "DenunciaEspecial--Descripcion:"
				+super.getdescripcion() + ", Fecha: "
				+super.getfecha() + ", Hora: "
				+super.gethora() + ", Latitud: "
				+getlatitud() + ", Longitud: " 
				+getlongitud();
}
	@Override
	public void setlatitud(double latitud) {
		this.latitud = latitud;
	}
	
	
	@Override
	public void setlongitud(double longitud) {
		this.longitud = longitud;
	}
}

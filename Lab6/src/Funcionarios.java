import java.util.ArrayList;

public class Funcionarios extends Usuario {
	private int carnetinstitucion;
	private String puesto;
	
	
	public int getcarnet() {
		return carnetinstitucion;
	}


	public String getpuesto() {
		return puesto;
	}
	
	
	public Funcionarios() {
	}
	
	public Funcionarios(String nombre, int id, int cedula, int contraseña, int carnetinstitucion, String puesto) {
		super(nombre, id, cedula, contraseña);
		this.carnetinstitucion = carnetinstitucion;
		this.puesto = puesto;
		setdenuncia(new ArrayList<Denuncias>());
	}

	
	
	
	@Override
		public String toString() {
			return "Funcionario: "
				+super.getnombre() + ", Cedula: "
				+super.getcedula() + ", ID: "
				+super.getid() + ", Contraseña: "
				+super.getcontraseña() + ", Puesto: "
				+getpuesto() + ", Carnet: "
				+getcarnet();
	}

	
	@Override
		public void setpuesto(String puesto) {
		this.puesto = puesto ;
	}
	
	@Override
		public void setcarnetinstitucion(int carnetinstitucion) {
		this.carnetinstitucion = carnetinstitucion;
	}
}

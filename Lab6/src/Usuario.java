import java.util.ArrayList;

public abstract class Usuario {
	
	
	public static int cantidadusuarios = 1;
	public static int contras = 10;
	
	protected String nombre;
	protected int id;
	protected int cedula;
	protected int contraseña;
	private ArrayList<Denuncias> denuncia;
	
	public ArrayList<Denuncias> getdenuncia() {
		return denuncia;
	}
	
	public void setdenuncia(ArrayList<Denuncias> denuncia) {
		this.denuncia = denuncia;
	}
	
	
	public void setnombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void setcedula(int cedula) {
		this.cedula = cedula;
	}
	
	public void setcontraseña() {
		this.contraseña = contras++;
	}
	
	public void setid() {
		this.id = cantidadusuarios++;
	}
	
	public String getnombre() {
		return this.nombre;
	}
	
	public int getcedula() {
		return this.cedula;
	}
	
	
	public int getid() {
		return this.id;
	}
	
	public int getcontraseña() {
		return this.contraseña;
	}
	
	abstract public String toString();
	
	public Usuario() {
		id = cantidadusuarios++;
		contraseña = contras++;
	}
	
	public Usuario(String nombre, int id, int cedula, int contraseña) {
		this.nombre = nombre;
		id = cantidadusuarios++;
		contraseña = contras++;
		this.cedula = cedula;
	}

	abstract public void setpuesto(String puesto);

	abstract public void setcarnetinstitucion(int carnetinstitucion);
}

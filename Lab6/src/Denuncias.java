
public abstract class Denuncias {
	
	protected String descripcion;
	protected int fecha;
	protected int hora;

	
	public void setdescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public void setfecha(int fecha) {
		this.fecha = fecha;
	}
	
	public void sethora(int hora) {
		this.hora = hora;
	}
	
	public String getdescripcion() {
		return this.descripcion;
	}
	
	public int getfecha() {
		return this.fecha;
	}
	
	public int gethora() {
		return this.hora;
	}

	public Denuncias() {
	}
	
	public Denuncias(String descripcion, int fecha, int hora) {
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.hora = hora;
	}
	
	public abstract void setlatitud(double latitud);
	
	public abstract void setlongitud(double longitud);
	
}
